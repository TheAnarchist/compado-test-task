### Remarks (questions from my side):
- I didn't find few required fields in response from Ebay: information about merchant, link to picture and brand. Maybe these fields present in production mode, or they need to be filled from other source or call.

### What can be improved:
- Handle error response from Ebay API
- Code coverage: other classes are testable (more or less)
- Filtering and sanitizing incoming parameters (keywords, filters)

### What I'd like to add:
- Dockerisation
- Simple CI/CD pipeline (lint, tests)
- Logging
- Authorization (?)

### Bonus questions:
> sometimes requesting data for the keyword takes a long time (> 5 seconds), what can be done to make it faster? What are downsides of your solution?

The first request will be slow, but we can increase speed of further requests by adding cache layer. The drawback of this solution is not up-to-date result, but it can be avoided with small ttl.

Another way to decrease response time is to paginate results. As drawback, we can rely on filters only for 1 page (they are working on our side).

> we’d like to add as many product feeds as possible, how would you structure a service with this requirement?

If I get it right, I'll try to use Strategy Pattern here.
