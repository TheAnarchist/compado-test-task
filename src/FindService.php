<?php
declare(strict_types = 1);

namespace App;

use GuzzleHttp\Client;

/**
 * Class FindService
 */
class FindService
{
    private Client $client;

    private EnvResolver $env;

    private SortProducts $sorting;

    /**
     * @param Client $client
     * @param EnvResolver $env
     * @param SortProducts $sorting
     */
    public function __construct(Client $client, EnvResolver $env, SortProducts $sorting)
    {
        $this->client = $client;
        $this->env = $env;
        $this->sorting = $sorting;
    }

    /**
     * @param array $request
     *
     * @return FoundProduct[]
     */
    public function find(array $request): array
    {
        // TODO: handle error in response
        $response = $this->client->request(
            'GET',
            $this->env->getSearchEndpoint(),
            $this->getQuery($request)
        );

        $content = $response->getBody()->getContents();
        $products = $this->parseResponse($content);

        return $this->applySort($products);
    }

    /**
     * @param array $request
     *
     * @return array
     */
    private function getQuery(array $request): array
    {
        $keywords = $this->getKeywords($request);
        $filters = $this->getFilters($request);

        $builder = (new QueryBuilder())
            ->withEnv($this->env)
            ->withKeywords($keywords);

        foreach ($filters as $filter) {
            $builder = $builder->withFilter($filter);
        }

        return [
            'query' => $builder->build(),
        ];
    }

    /**
     * @param array $request
     *
     * @return string
     */
    private function getKeywords(array $request): string
    {
        if (false === isset($request['keywords'])) {
            throw new \RuntimeException('Keywords are not set for request');
        }

        return $request['keywords'];
    }

    /**
     * @param array $request
     *
     * @return SearchFilter[]
     */
    private function getFilters(array $request): array
    {
        $filters = [];

        if (true === isset($request['price_min'])) {
            $filters[] = SearchFilter::createMinPriceFilter($request['price_min']);
        }

        if (true === isset($request['price_max'])) {
            $filters[] = SearchFilter::createMaxPriceFilter($request['price_max']);
        }

        return $filters;
    }

    /**
     * @param string $xmlString
     *
     * @return FoundProduct[]
     */
    private function parseResponse(string $xmlString): array
    {
        $element = new \SimpleXMLElement($xmlString);
        $products = [];

        foreach ($element->searchResult->item as $item) {
            $products[] = new FoundProduct($item);
        }

        return $products;
    }

    /**
     * @param FoundProduct[] $products
     *
     * @return FoundProduct[]
     */
    private function applySort(array $products): array
    {
        if (!$this->sorting->isRequestHasSortingKey()) {
            return $products;
        }

        if (!$this->sorting->isSortingAllowed()) {
            return $products;
        }

        if ($this->sorting->isDefaultSorting()) {
            return $products;
        }

        return $this->sorting->apply($products);
    }
}
