<?php
declare(strict_types=1);

namespace App;

/**
 * Class SearchFilter
 */
class SearchFilter
{
    public const KEY_MAX_PRICE = 'MaxPrice';
    public const KEY_MIN_PRICE = 'MinPrice';
    public const DEFAULT_PARAM_NAME = 'Currency';
    public const DEFAULT_PARAM_VALUE = 'USD';

    private string $name;
    private string $value;
    private string $paramName;
    private string $paramValue;

    /**
     * @param string $name
     * @param string $value
     * @param string $paramName
     * @param string $paramValue
     */
    public function __construct(
        string $name,
        string $value,
        string $paramName,
        string $paramValue
    ) {
        $this->name = $name;
        $this->value = $value;
        $this->paramName = $paramName;
        $this->paramValue = $paramValue;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string|null
     */
    public function getParamName(): ?string
    {
        return $this->paramName;
    }

    /**
     * @return string|null
     */
    public function getParamValue(): ?string
    {
        return $this->paramValue;
    }

    /**
     * @param string $value
     *
     * @return SearchFilter
     */
    public static function createMinPriceFilter(string $value): SearchFilter
    {
        return new self(
            self::KEY_MIN_PRICE,
            $value,
            self::DEFAULT_PARAM_NAME,
            self::DEFAULT_PARAM_VALUE
        );
    }

    /**
     * @param string $value
     *
     * @return SearchFilter
     */
    public static function createMaxPriceFilter(string $value): SearchFilter
    {
        return new self(
            self::KEY_MAX_PRICE,
            $value,
            self::DEFAULT_PARAM_NAME,
            self::DEFAULT_PARAM_VALUE
        );
    }
}
