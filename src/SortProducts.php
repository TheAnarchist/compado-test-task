<?php
declare(strict_types = 1);

namespace App;

/**
 * Class SortProducts
 */
class SortProducts
{
    private const ALLOWED_SORTING = [
        'default' => '',
        'by_price_asc' => 'sortByPriceAsc',
    ];

    private const DEFAULT_SORTING = 'default';

    private array $request;

    /**
     * @param array $request
     */
    public function __construct(array $request)
    {
        $this->request = $request;
    }

    public function isRequestHasSortingKey(): bool
    {
        return true === isset($this->request['sorting']);
    }

    /**
     * @return bool
     */
    public function isSortingAllowed(): bool
    {
        return array_key_exists($this->request['sorting'], self::ALLOWED_SORTING);
    }

    /**
     * @return bool
     */
    public function isDefaultSorting(): bool
    {
        $sorting = $this->request['sorting'];

        return (self::DEFAULT_SORTING === $sorting);
    }

    /**
     * @param FoundProduct[] $products
     *
     * @return FoundProduct[]
     */
    public function apply(array $products): array
    {
        $sorting = $this->request['sorting'];
        $method = [$this, self::ALLOWED_SORTING[$sorting]];

        usort($products, $method);

        return $products;
    }

    /**
     * @param FoundProduct $p1
     * @param FoundProduct $p2
     *
     * @return int
     */
    private function sortByPriceAsc(FoundProduct $p1, FoundProduct $p2): int
    {
        return $p1->price <=> $p2->price;
    }
}
