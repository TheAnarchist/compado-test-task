<?php
declare(strict_types = 1);

namespace App;

/**
 * Class FoundProduct
 */
class FoundProduct
{
    public string $provider = 'ebay';
    public string $merchantId = '???';
    public string $merchantLogoUrl = '???';
    public string $itemId;
    public string $clickOutLink;
    public string $mainPhotoUrl = '???';
    public float  $price;
    public string $priceCurrency;
    public float  $shippingPrice;
    public string $title;
    public string $validUntil;
    public string $brand = '???';

    /**
     * @param \SimpleXMLElement $element
     */
    public function __construct(\SimpleXMLElement $element)
    {
        $this->itemId = (string) $element->itemId;
        $this->title = (string) $element->title;

        $this->clickOutLink = (string) $element->viewItemURL;

        $this->price = (float) $element->sellingStatus->currentPrice;
        $this->priceCurrency = (string) $element->sellingStatus->currentPrice['currencyId'];

        $this->shippingPrice = (float) $element->shippingInfo->shippingServiceCost;

        $this->validUntil = (string) $element->listingInfo->endTime;
    }
}
