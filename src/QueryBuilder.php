<?php
declare(strict_types = 1);

namespace App;

/**
 * Class QueryBuilder
 */
class QueryBuilder
{
    private const RESPONSE_FORMAT = 'XML';

    private EnvResolver $env;

    private string $keywords;

    /**
     * @var SearchFilter[]
     */
    private array $filters;

    /**
     * @param EnvResolver $env
     *
     * @return $this
     */
    public function withEnv(EnvResolver $env): QueryBuilder
    {
        $this->env = $env;

        return $this;
    }

    /**
     * @param string $keywords
     *
     * @return $this
     */
    public function withKeywords(string $keywords): QueryBuilder
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function withFilter(SearchFilter $filter): QueryBuilder
    {
        $this->filters[] = $filter;

        return $this;
    }

    /**
     * @return array
     */
    public function build(): array
    {
        $serviceQuery = [
            'OPERATION-NAME' => $this->env->getOperationName(),
            'SERVICE-VERSION' => $this->env->getServiceVersion(),
            'RESPONSE-DATA-FORMAT' => self::RESPONSE_FORMAT,
            'SECURITY-APPNAME' => $this->env->getAppName(),
            'GLOBAL-ID' => $this->env->getGlobalId(),
            'keywords' => $this->keywords,
        ];

        return array_merge($serviceQuery, $this->transformFilters());
    }

    /**
     * @return array
     */
    private function transformFilters(): array
    {
        if (1 === count($this->filters)) {
            return $this->transformOneFilter($this->filters[0]);
        }

        $result = [];
        foreach ($this->filters as $i => $filter) {
            $keyBase = 'itemFilter(' . $i . ')';
            $result[$keyBase . '.name'] = $filter->getName();
            $result[$keyBase . '.value'] = $filter->getValue();
            $result[$keyBase . '.paramName'] = $filter->getParamName();
            $result[$keyBase . '.paramValue'] = $filter->getParamValue();
        }

        return $result;
    }

    /**
     * @param SearchFilter $filter
     *
     * @return array
     */
    private function transformOneFilter(SearchFilter $filter): array
    {
        return [
            'itemFilter.name' => $filter->getName(),
            'itemFilter.value' => $filter->getValue(),
            'itemFilter.paramName' => $filter->getParamName(),
            'itemFilter.paramValue' => $filter->getParamValue(),
        ];
    }
}
