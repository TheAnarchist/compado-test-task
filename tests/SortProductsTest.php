<?php
declare(strict_types = 1);

use App\SortProducts;
use App\FoundProduct;
use PHPUnit\Framework\TestCase;

/**
 * Class SortProductsTest
 */
class SortProductsTest extends TestCase
{
    /**
     * @var FoundProduct[]
     */
    private array $originProducts = [];

    public function setUp(): void
    {
        parent::setUpBeforeClass();

        $this->originProducts = [
            $this->createProduct(100),
            $this->createProduct(10),
        ];
    }

    /**
     * @dataProvider dataIsRequestHasSortingKey
     *
     * @param array $request
     * @param bool $expected
     */
    public function testIsRequestHasSortingKey(array $request, bool $expected): void
    {
        $sorting = new SortProducts($request);

        $actual = $sorting->isRequestHasSortingKey();
        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider dataIsSortingAllowed
     *
     * @param array $request
     * @param bool $expected
     */
    public function testIsSortingAllowed(array $request, bool $expected): void
    {
        $sorting = new SortProducts($request);

        $actual = $sorting->isSortingAllowed();
        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider dataIsDefaultSorting
     *
     * @param array $request
     * @param bool $expected
     */
    public function testIsDefaultSorting(array $request, bool $expected): void
    {
        $sorting = new SortProducts($request);

        $actual = $sorting->isDefaultSorting();
        $this->assertEquals($expected, $actual);
    }

    /**
     * @return Generator
     */
    public function dataIsRequestHasSortingKey(): Generator
    {
        // Request without sorting key
        yield [
            [],
            false,
        ];

        // Request with default sorting
        yield [
            ['sorting' => 'default'],
            true,
        ];

        // Request with not allowed sorting key
        yield [
            ['sorting' => 'by_title'],
            true,
        ];
    }

    /**
     * @return Generator
     */
    public function dataIsSortingAllowed(): Generator
    {
        // Request with not allowed sorting key
        yield [
            ['sorting' => 'by_title'],
            false,
        ];

        // Request with default sorting
        yield [
            ['sorting' => 'default'],
            true,
        ];
    }

    /**
     * @return Generator
     */
    public function dataIsDefaultSorting(): Generator
    {
        // Request with default sorting
        yield [
            ['sorting' => 'default'],
            true,
        ];

        // Request with allowed sorting key
        yield [
            ['sorting' => 'by_price_asc'],
            false,
        ];

        // Request with not allowed sorting key
        yield [
            ['sorting' => 'by_title'],
            false,
        ];
    }

    /**
     * @param float $price
     *
     * @return FoundProduct
     */
    private function createProduct(float $price): FoundProduct
    {
        $product = $this->createMock(FoundProduct::class);
        $product->price = $price;

        return $product;
    }
}
