<?php
declare(strict_types = 1);

use App\SearchFilter;
use PHPUnit\Framework\TestCase;

/**
 * Class SearchFilterTest
 */
class SearchFilterTest extends TestCase
{
    private const VALUE = '100';

    public function testCreateMaxPriceFilter(): void
    {
        $filter = SearchFilter::createMaxPriceFilter(self::VALUE);

        $this->assertSame(self::VALUE, $filter->getValue());
        $this->assertSame(SearchFilter::KEY_MAX_PRICE, $filter->getName());
        $this->assertSame(SearchFilter::DEFAULT_PARAM_NAME, $filter->getParamName());
        $this->assertSame(SearchFilter::DEFAULT_PARAM_VALUE, $filter->getParamValue());
    }

    public function testCreateMinPriceFilter(): void
    {
        $filter = SearchFilter::createMinPriceFilter(self::VALUE);

        $this->assertSame(self::VALUE, $filter->getValue());
        $this->assertSame(SearchFilter::KEY_MIN_PRICE, $filter->getName());
        $this->assertSame(SearchFilter::DEFAULT_PARAM_NAME, $filter->getParamName());
        $this->assertSame(SearchFilter::DEFAULT_PARAM_VALUE, $filter->getParamValue());
    }
}
