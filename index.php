<?php
declare(strict_types = 1);

require 'vendor/autoload.php';

use Dotenv\Dotenv;
use App\EnvResolver;
use App\SortProducts;
use GuzzleHttp\Client;
use App\FindService;

$dotEnv = Dotenv::createImmutable(__DIR__);
$dotEnv->load();

$env = new EnvResolver();
$sorting = new SortProducts($_REQUEST);
$client = new Client(['base_uri' => $env->getBaseUrl(),]);

$service = new FindService($client, $env, $sorting);
$products = $service->find($_REQUEST);

header('Content-type: application/json');
echo json_encode($products);
